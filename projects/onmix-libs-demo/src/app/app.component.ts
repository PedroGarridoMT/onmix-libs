import { Component } from '@angular/core';
import { OnMixUtilsService} from 'onmix-libs';

@Component({
  selector: 'onmix-demo-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project-angular';

  constructor(
    private utilsService: OnMixUtilsService
  ) {
    console.log(this.utilsService.getMocks());
    this.title = this.utilsService.getMocks();
  }
}
